package com.example.peter.todolistapplication;

import android.view.View;
import android.widget.TextView;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DataViewHolder extends RecyclerView.ViewHolder  {

    public TextView tvNote,tvTitle,tvDate;
    public View mView;

    public DataViewHolder(@NonNull View itemView) {
        super(itemView);
        mView = itemView;
    }

    public void setTitle(String title){
        tvTitle = mView.findViewById(R.id.tv_title);
        tvTitle.setText(title);
    }

    public void setNote(String note){
        tvNote = mView.findViewById(R.id.tv_note);
        tvNote.setText(note);
    }

    public void setDate(String Date){
        tvDate = mView.findViewById(R.id.tv_date);
        tvDate.setText(Date);
    }
}
