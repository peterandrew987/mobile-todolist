package com.example.peter.todolistapplication;

public class Data {
    private String mTitle,mNote,mDate,mId;

    public Data(){}

    public Data(String title, String note, String date, String id) {
        this.mTitle = title;
        this.mNote = note;
        this.mDate = date;
        this.mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        this.mNote = note;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        this.mDate = date;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

}
