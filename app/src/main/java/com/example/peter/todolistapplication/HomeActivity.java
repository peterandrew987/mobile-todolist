package com.example.peter.todolistapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.text.DateFormat;
import java.util.Date;


public class HomeActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private AlertDialog mDialog;
    private EditText title;
    private EditText note;
    private Button btnSave;
    private View mView;
    private String uId;
    private RecyclerView recyclerView;
    private EditText titleUp, noteUp;
    private Button btnUpdate, btnDelete;
    //Firebase

    private DatabaseReference mReferences;
    private FirebaseAuth mAuth;
    private FirebaseRecyclerAdapter<Data, DataViewHolder> adapter;

    private String dataTitle, dataNote, dataPost_key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        fab = findViewById(R.id.btn_float_edit);
        toolbar = findViewById(R.id.toolbar_home);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("My Task App");

        mAuth = FirebaseAuth.getInstance();
        uId = mAuth.getUid();
        mReferences = FirebaseDatabase.getInstance().getReference().child("TaskNote").child(uId);
        recyclerView = findViewById(R.id.recyler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        fetch();
        recyclerView.setAdapter(adapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater inflater = LayoutInflater.from(HomeActivity.this);
                mView = inflater.inflate(R.layout.custominputfield, null);
                mDialogBuilder.setView(mView);
                mDialog = mDialogBuilder.create();

                title = mView.findViewById(R.id.et_title);
                note = mView.findViewById(R.id.et_note);
                btnSave = mView.findViewById(R.id.btn_save);
                mDialog.show();
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "bisa kok", Toast.LENGTH_LONG).show();

                        String mTitle = title.getText().toString().trim();
                        String mNote = note.getText().toString().trim();

                        if (TextUtils.isEmpty(mTitle)) {
                            title.setError("Required Field");
                            return;
                        }
                        if (TextUtils.isEmpty(mNote)) {
                            note.setError("Required Field");
                            return;
                        }

                        String id = mReferences.push().getKey();
                        String date = DateFormat.getDateInstance().format(new Date());
                        Data data = new Data(mTitle, mNote, date, id);

                        mReferences.child(id).setValue(data);

                        Toast.makeText(getApplicationContext(), "Data insert", Toast.LENGTH_LONG).show();

                        mDialog.dismiss();
                    }
                });

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.startListening();
    }

    private void fetch() {
        Query query = mReferences;

        FirebaseRecyclerOptions<Data> options =
                new FirebaseRecyclerOptions.Builder<Data>()
                        .setQuery(query, Data.class)
                        .build();

        adapter = new FirebaseRecyclerAdapter<Data, DataViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull DataViewHolder dataViewHolder, final int index, @NonNull final Data data) {
                dataViewHolder.setDate(data.getDate());
                dataViewHolder.setNote(data.getNote());
                dataViewHolder.setTitle(data.getTitle());
                dataViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dataPost_key = getRef(index).getKey();
                        dataTitle = data.getTitle();
                        dataNote = data.getNote();

                        updateDate();


                    }
                });
            }

            @NonNull
            @Override
            public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_data, parent, false);

                return new DataViewHolder(view);
            }
        };
    }

    public void updateDate() {

        AlertDialog.Builder builderUpdateDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(HomeActivity.this);
        View mView = inflater.inflate(R.layout.updateinputfield, null);
        builderUpdateDialog.setView(mView);
        final AlertDialog updateDialog = builderUpdateDialog.create();

        titleUp = mView.findViewById(R.id.et_titleupdate);
        noteUp = mView.findViewById(R.id.et_noteupdate);

        titleUp.setText(dataTitle);
        titleUp.setSelection(dataTitle.length());

        noteUp.setText(dataNote);
        noteUp.setSelection(dataNote.length());

        btnDelete = mView.findViewById(R.id.btn_update_delete);
        btnUpdate = mView.findViewById(R.id.btn_update_update);


        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataTitle = titleUp.getText().toString().trim();
                dataNote = noteUp.getText().toString().trim();

                String mDate = DateFormat.getDateInstance().format(new Date());

                Data data = new Data(dataTitle,dataNote,mDate,dataPost_key);

                mReferences.child(dataPost_key).setValue(data);

                updateDialog.dismiss();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mReferences.child(dataPost_key).removeValue();

                updateDialog.dismiss();
            }
        });

        updateDialog.show();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.log_out:
                mAuth.signOut();
                AlertDialog.Builder bAlertDialog = new AlertDialog.Builder(this);
                bAlertDialog.setTitle("Log Out");
                bAlertDialog.setMessage("Are you sure to Log out ?");
                bAlertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    }
                });
                bAlertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                bAlertDialog.setCancelable(true);

                bAlertDialog.create().show();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
